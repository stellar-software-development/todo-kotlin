package com.example.todokotlin

data class ToDo(val id: Int, var completed: Boolean, val title: String)