package com.example.todokotlin

import android.view.View
import android.widget.CheckBox
import android.widget.CheckedTextView

class CheckboxUiHandler {
    lateinit var checkbox: CheckedTextView;

    constructor(id: Int, view: View, checked: Boolean, text: String) {
        checkbox = view.findViewById(id) as CheckedTextView
        checkbox.setChecked(checked)
        checkbox!!.jumpDrawablesToCurrentState()

        checkbox.text = text;
    }

    fun setListener(onClick: () -> Unit) {
        checkbox!!.setOnClickListener {
            checkbox.setChecked(!checkbox!!.isChecked)
            checkbox!!.jumpDrawablesToCurrentState()
            onClick.invoke()
        }
    }
}