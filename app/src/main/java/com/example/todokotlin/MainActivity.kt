package com.example.todokotlin

import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONException

class MainActivity : AppCompatActivity() {
    private lateinit var toDoItemsList: ListView
    private var toDos: ArrayList<ToDo> = ArrayList<ToDo>()
    private val toDoApiHandler = ToDoApiHandler();
    private val toDoUiHandler = ToDoUIHandler();

    var completedCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toDoUiHandler.createButton(R.id.addToDoItemButton, findViewById(R.id.main_activity)) {
            this@MainActivity.createToDoItem()
        }
        toDoUiHandler.createButton(R.id.uncheck_all_button, findViewById(R.id.main_activity)) {
            this@MainActivity.uncheckAll()
        }

        toDoUiHandler.createRadioGroup(R.id.radio_group, findViewById(R.id.main_activity)) {
            this@MainActivity.populateView(toDos)
        }
        toDoItemsList = findViewById<ListView>(R.id.toDoList)

        this.getAllToDos()
    }

    private fun getFilter() : Boolean? {
        val radioGroup: RadioGroup = findViewById(R.id.radio_group)
        val radio: RadioButton = findViewById(radioGroup.checkedRadioButtonId)
        var selectedRadioButton: CharSequence = radio.text
        var isCompleted: Boolean? =  null
        when(selectedRadioButton) {
            "All" -> isCompleted = null
            "Active" -> isCompleted = false
            "Completed" -> isCompleted = true
        }
        return isCompleted;
    }

    private fun populateView(showToDos: ArrayList<ToDo>) {
        val isCompleted = getFilter();
        toDos = showToDos;
        val filterToDos = toDoApiHandler.filterToDos(isCompleted, toDos);
        this.populateListView(filterToDos)
        this.completedCount = toDoApiHandler.countTodos(toDos)
        val countPlainText = findViewById(R.id.complited_count) as TextView
        countPlainText.text = this.completedCount.toString()
    }

     fun getAllToDos() {
        toDoApiHandler.getAllTodos {
            runOnUiThread {
                try {
                    this@MainActivity.populateView(it)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun uncheckAll() {
        toDoApiHandler.uncheckAllToDos(toDos, { this@MainActivity.getAllToDos() })
    }

    private fun populateListView(toDoList: ArrayList<ToDo>) {
        val adapter = ToDoAdapter(this, toDoList, toDoApiHandler, toDoUiHandler)
        toDoItemsList.adapter = adapter
    }

    private fun createToDoItem() {
        val toDoTextInput = findViewById(R.id.toDoTextInput) as EditText
        val toDoValue = toDoTextInput.text.toString();
        toDoApiHandler.createToDo(toDoValue) {
            runOnUiThread {
                try {
                    this@MainActivity.getAllToDos()
                    toDoTextInput.setText("")
                    Utils.hideSoftKeyBoard(this@MainActivity, findViewById(R.id.main_activity) )
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        }
    }
}
