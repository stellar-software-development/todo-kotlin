package com.example.todokotlin

import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONException
import java.io.IOException

class ToDoApiHandler {
    private val url = "http://46.101.198.198:3000/todo/"
    private val client = OkHttpClient()
    var request = HttpRequest(client)

    fun filterToDos(isComplited: Boolean?, toDos: ArrayList<ToDo>): ArrayList<ToDo> {
        if (isComplited != null) {
            return ArrayList(toDos.filter { value: ToDo -> value.completed == isComplited})
        }
       return toDos;
    }

    fun uncheckAllToDos(toDos: ArrayList<ToDo>, callback: () -> Unit) {
        val uncheckToDos = ArrayList(toDos.filter { value: ToDo -> value.completed == true})
        for (toDo in uncheckToDos) {
            val map: HashMap<String, String> = hashMapOf("completed" to false.toString(), "title" to toDo.title)
            println(map.toString())
            request.PATCH(url+toDo.id, map, object: Callback {

                override fun onResponse(call: Call?, response: Response) {
                    println(response.toString())
                    callback.invoke();
                }

                override fun onFailure(call: Call?, e: IOException?) {
                    println(e.toString())
                    println("Request Failure.")
                }
            })
        }
    }

    fun getAllTodos(callback: (result: ArrayList<ToDo>) -> Unit) {
        request.GET(url, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                val responseData = response.body()?.string()
                var json = JSONArray(responseData)
                var toDos = ArrayList<ToDo>()
                for (i in 0 until json.length()) {
                    print(i.toString())
                    toDos.add(ToDo(json.getJSONObject(i).get("id").toString().toInt(), json.getJSONObject(i).get("completed").toString().toBoolean(), json.getJSONObject(i).get("title").toString()))
                }

                callback.invoke(toDos);
            }

            override fun onFailure(call: Call?, e: IOException?) {
                println("Request Failure.")
            }
        })
    }


    fun createToDo(toDoValue: String, callback: () -> Unit) {
        val map: HashMap<String, String> = hashMapOf("order" to "1", "title" to toDoValue, "completed" to "false")
        request.POST(url, map, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
               callback.invoke()
            }

            override fun onFailure(call: Call?, e: IOException?) {
                println(e.toString())
                println("Request Failure.")
            }
        })
    }

    fun changeToDoStatus (toDo: ToDo, callback: () -> Unit) {
        val map: HashMap<String, String> = hashMapOf("completed" to (!toDo.completed).toString(), "title" to toDo.title)
        println(map.toString())
        request.PATCH(url+toDo.id, map, object: Callback {
            override fun onResponse(call: Call?, response: Response) {
                callback.invoke()
            }

            override fun onFailure(call: Call?, e: IOException?) {
                println(e.toString())
                println("Request Failure.")
            }
        })
    }

    fun deleteToDo(id: Int, callback: () -> Unit) {
        request.DELETE(url+id, object: Callback {
            override fun onResponse(call: Call, response: Response) {
                callback.invoke()
            }
            override fun onFailure(call: Call, e: IOException) {
                print(e.toString())
            }
        })
    }

    fun countTodos(toDos: ArrayList<ToDo>): Int {
        return toDos.count { value: ToDo -> value.completed == true}
    }
}