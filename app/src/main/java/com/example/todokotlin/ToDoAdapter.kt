package com.example.todokotlin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.CheckedTextView
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException
import java.lang.Exception

class ToDoAdapter(private val context: Context, private val dataSource: ArrayList<ToDo>, toDoApiHandler: ToDoApiHandler, toDoUIHandler: ToDoUIHandler) : BaseAdapter() {
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val handler: ToDoApiHandler = toDoApiHandler
    private val uiHandler: ToDoUIHandler = toDoUIHandler

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.todo_item, parent, false)

        try {
            val toDo = getItem(position) as ToDo
            uiHandler.createCheckbox(toDo, rowView) { handler.changeToDoStatus(it) {(context as MainActivity).getAllToDos()} };
            uiHandler.createButton(R.id.delete_button, rowView) { handler.deleteToDo(toDo.id) {(context as MainActivity).getAllToDos()} }
            return rowView
        } catch (e: Exception) {
            println(e.toString())
        }
        return rowView
    }
}