package com.example.todokotlin

import android.view.View
import android.widget.Button
import android.widget.CheckedTextView
import android.widget.RadioGroup

class ToDoUIHandler {
    fun createButton(id: Int, view: View, onClick:() -> Unit) {
        val button = view.findViewById(id) as Button
        button.setOnClickListener { onClick.invoke() }
    }

    fun createCheckbox(toDo: ToDo, rowView: View, toggleCheckbox: (result: ToDo) -> Unit) {
        val checkbox = CheckboxUiHandler(R.id.task_title, rowView, toDo.completed, toDo.title)
        checkbox.setListener {
            toDo.completed = !checkbox.checkbox.isChecked
            toggleCheckbox.invoke(toDo) }
    }

    fun createRadioGroup(id: Int, view: View, onClick: () -> Unit) {
        val radioGroup:RadioGroup = view.findViewById(id)
        radioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                onClick.invoke()
            })
    }
}